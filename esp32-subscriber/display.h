
#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

enum message_t {NONE, WELCOME, NO_CARD, UNKNOWN_CARD, NO_CREDIT, INACTIVE_CARD, ENTER, ASK_FOR_CARD};

class display
{
  protected:
    TaskHandle_t m_mainTask;
  public:
    display();
    virtual void begin();
    virtual void clear() {};
    virtual void displayWelcome(void) {};
    virtual void displayNoCard(void) {};
    virtual void displayUnknownCard(void) {};
    virtual void displayNoCredit(void) {};
    virtual void displayInactiveCard(void) {};
    virtual void displayEnter(int credit) {};
    virtual void displayAskForCard(void) {};
    void process();
};

class lcdDisplay : public display
{
  private:
    LiquidCrystal_I2C lcd;

  public:
    lcdDisplay();
    void begin();
    void clear();
    void displayWelcome(void);
    void displayNoCard(void);
    void displayUnknownCard(void);
    void displayNoCredit(void);
    void displayInactiveCard(void);
    void displayEnter(int credit);
    void displayAskForCard(void);
};

class oledDisplay : public display
{
  private:
    Adafruit_SSD1306 oled;

  public:
    oledDisplay();
    void begin();
    void clear();
    void displayWelcome(void);
    void displayNoCard(void);
    void displayUnknownCard(void);
    void displayNoCredit(void);
    void displayInactiveCard(void);
    void displayEnter(int credit);
    void displayAskForCard(void);
};

#endif
