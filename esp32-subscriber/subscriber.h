
#ifndef __SUBSCRIBERS_H__
#define __SUBSCRIBERS_H__

#define NO_ERROR          0
#define ERR_NOCARD        -1
#define ERR_READCARD      -2
#define ERR_CARDTYPE      -3
#define ERR_NOTFOUND      -4
#define ERR_OPEN          -5
#define ERR_WRITE         -6
#define ERR_FATFULL       -7
#define ERR_GETTIME       -8

#define SUBSCRIBERS_FILE  "/subs"

class Subscriber
{
  public:
    Subscriber(uint32_t id, const char *name, bool active, unsigned long credit, const char *history);
    static int getTotalSubscribers(void);
    static int readRFID(uint32_t *uid);
    static int readMedia(void);
    static int newSubscriber(const char*name);
    static int addSubscriber(const char*name, uint32_t uid);
    static int fromId(uint32_t id);
    static int fromName(const char *name);
    static void clearAll(void);
    static void begin(void);
    static Subscriber *fromIndex(int index);
    static int saveToFat(void);
    int getIndex()            {return m_index;}
    int getId(void)           {return m_id;}
    const char *getName(void) {return m_name.c_str();}
    int isActive(void)        {return m_active;}
    int getCredit(void)       {return m_credit;}
    String getHistory(void)   {return m_history;}
    int activate(void);
    int deactivate(void);
    int addCredit(int n);
    int resetCredit(void);
    int addToHistory(String s);

  private:
    static int s_index;
    int m_index;
    uint32_t m_id;
    String m_name;
    bool m_active;
    int m_credit;
    String m_history;
};

#endif
