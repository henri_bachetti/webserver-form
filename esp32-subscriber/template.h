
#ifndef __TEMPLATE_H__
#define __TEMPLATE_H__

#include <Arduino.h>

class DictionaryEntry
{
  public:
    DictionaryEntry(const char *name, const char *value);
    String m_name;
    String m_value;
};

void addToDictionary(const char *name, long value);
void addToDictionary(const char *name, const char *value);
const char *getDictionaryValue(const char *name);
void clearDictionary(void);

#endif
