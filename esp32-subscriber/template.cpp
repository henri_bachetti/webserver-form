
#include <list>

#include "template.h"

DictionaryEntry::DictionaryEntry(const char *name, const char *value) :
  m_name(name),
  m_value(value)
{
}

std::list<DictionaryEntry> dictionary;

void addToDictionary(const char *name, long value)
{
  char s[11];
  ltoa(value, s, 10);
  dictionary.push_back(DictionaryEntry(name, s));
}

void addToDictionary(const char *name, const char *value)
{
  dictionary.push_back(DictionaryEntry(name, value));
}

const char *getDictionaryValue(const char *name)
{
  std::list<DictionaryEntry>::iterator it;

  for (it = dictionary.begin() ; it != dictionary.end() ; ++it) {
    if (it->m_name == name) {
      return it->m_value.c_str();
    }
  }
  return "????";
}

void clearDictionary(void)
{
  dictionary.clear();
}
