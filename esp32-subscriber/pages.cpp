
#include <SPIFFS.h>

#include "template.h"
#include "pages.h"

#define RESPONSE_SIZE     1950
char msg[RESPONSE_SIZE];

#if METHOD == USE_SPRINTF

/*
   subscribers form as C string with format strings
   Format :
   %s : date & time
   %d : current subscriber number
   %d : number of subscribers
   %d : current subscriber number
   %d : credits
   %s : name
   %s : history
   %s : status
   %s : status color
*/

const char *subscribersForm = "<!DOCTYPE html>\n"
                              "<html>\n"
                              "<head>\n"
                              "<meta charset=\"UTF-8\">\n"
                              "<title>Subscribers</title>\n"
                              "</head>\n"
                              "<body>\n"
                              "<script type=\"text/javascript\">\n"
                              "function subscriberChanged(value){\n"
                              "  document.location =\"/?subscriber=\" + value;\n"
                              "};\n"
                              "function readMedia(){\n"
                              "  document.location =\"/?action=read_media\"\n"
                              "};\n"
                              "</script>\n"
                              "%s<br><br><h1>SUBSCRIBERS</h1>\n"
                              "<button onclick=\"readMedia()\">Read media</button>\n"
                              "<br>"
                              "<br>\n"
                              "<form action=\"/\" method=\"post\">\n"
                              "<br>\n"
                              "<label for=\"subscriber\">Subscriber number : </label>\n"
                              "<br>\n"
                              "<input type=\"number\" id=\"subscriber\" name=\"subscriber\" value=\"%d\" min=\"1\" max=\"%d\" onchange=\"subscriberChanged(this.value)\">\n"
                              "<br>"
                              "<br>\n"
                              "<label for=\"subscriber_id\">Subscriber ID : </label>\n"
                              "<br>\n"
                              "<input type=\"text\" id=\"subscriber_id\" name=\"subscriber_id\" value=\"%lx\" onchange=\"subscriberIdChanged(this.value)\">\n"
                              "<br>"
                              "<br>\n"
                              "<label for=\"status\">Subscriber name :</label>\n"
                              "<br>"
                              "<input type=\"text\" id=\"name\" name=\"name\" value=\"%s\">\n"
                              "<br>"
                              "<br>\n"
                              "<label>Subscriber : %d credits</label>\n"
                              "<br>"
                              "<br>\n"
                              "<input type=\"radio\" name=\"action\" value=\"add1\" checked>Add 1 credit\n"
                              "<br>\n"
                              "<input type=\"radio\" name=\"action\" value=\"add5\">Add 5 credits\n"
                              "<br>\n"
                              "<input type=\"radio\" name=\"action\" value=\"add10\">Add 10 credits\n"
                              "<br>\n"
                              "<input type=\"radio\" name=\"action\" value=\"reset\">Reset subscriber\n"
                              "<br>\n"
                              "<input type=\"radio\" name=\"action\" value=\"deactivate\">Deactivate subscriber\n"
                              "<br>\n"
                              "<input type=\"radio\" name=\"action\" value=\"activate\">Reactivate subscriber\n"
                              "<br>"
                              "<br>\n"
                              "<label for=\"history\">History :</label>\n"
                              "<br>\n"
                              "<textarea id=\"history\" rows=\"10\" cols=\"40\" name=\"history\">%s</textarea>\n"
                              "<br>"
                              "<br>\n"
                              "<label for=\"status\">Status :</label>\n"
                              "<br>"
                              "<input type=\"text\" id=\"status\" name=\"status\" value=\"%s\" readonly style=\"background-color:%s;color:white;\">\n"
                              "<br>"
                              "<br>\n"
                              "<input type=\"submit\" value=\"OK\">\n"
                              "</form>\n"
                              "</body>\n"
                              "</html>";

int SendSubscribersForm(AsyncWebServerRequest *request, const char *timeString, int subscriberIndex, int totalSubscribers, int subscriberId, const char *subscriberName,
                        int credit, const char *history, const char *status, const char *statusColor)
{
  unsigned long start = millis();
  snprintf(msg, RESPONSE_SIZE, subscribersForm, timeString, subscriberIndex, totalSubscribers, subscriberId, subscriberName, credit, history, status, statusColor);
  size_t sz = strlen(msg);
  request->send(200, "text/html", msg);
  if (sz > RESPONSE_SIZE - 50) {
    Serial.print("WARNING: response size: "); Serial.print(sz); Serial.print(" (maximal size: "); Serial.print(RESPONSE_SIZE); Serial.println(")");
  }
  Serial.print("response time: "); Serial.println(millis() - start);
}

#elif METHOD == USE_STRING

int SendSubscribersForm(AsyncWebServerRequest *request, const char *timeString, int subscriberIndex, int totalSubscribers, int subscriberId, const char *subscriberName,
                        int credit, const char *history, const char *status, const char *statusColor)
{
  unsigned long start = millis();
  String msg = "<!DOCTYPE html>\n";
  msg += "<html>\n";
  msg += "<head>\n";
  msg += "<meta charset=\"UTF-8\">\n";
  msg += "<title>Subscribers</title>\n";
  msg += "</head>\n";
  msg += "<body>\n";
  msg += "<script type=\"text/javascript\">\n";
  msg += "function subscriberChanged(value){\n";
  msg += "  document.location =\"/?subscriber=\" + value;\n";
  msg += "};\n";
  msg += "function readMedia(){\n";
  msg += "  document.location =\"/?action=read_media\"\n";
  msg += "};\n";
  msg += "</script>\n";
  msg += timeString;
  msg += "<br><br><h1>SUBSCRIBERS</h1>\n";
  msg += "<button onclick=\"readMedia()\">Read media</button>\n";
  msg += "<br>";
  msg += "<br>\n";
  msg += "<form action=\"/\" method=\"post\">\n";
  msg += "<br>\n";
  msg += "<label for=\"subscriber\">Subscriber number : </label>\n";
  msg += "<br>\n";
  msg += "<input type=\"number\" id=\"subscriber\" name=\"subscriber\" value=\"";
  msg += String(subscriberIndex);
  msg += "\" min=\"1\" max=\"";
  msg += String(totalSubscribers);
  msg += "\" onchange=\"subscriberChanged(this.value)\">\n";
  msg += "<br>";
  msg += "<br>\n";
  msg += "<label for=\"subscriber_id\">Subscriber ID : </label>\n";
  msg += "<br>\n";
  msg += "<input type=\"text\" id=\"subscriber_id\" name=\"subscriber_id\" value=\"";
  msg += String(subscriberId);
  msg += "\" onchange=\"subscriberIdChanged(this.value)\">\n";
  msg += "<br>";
  msg += "<br>\n";
  msg += "<label for=\"status\">Subscriber name :</label>\n";
  msg += "<br>";
  msg += "<input type=\"text\" id=\"name\" name=\"name\" value=\"";
  msg += subscriberName;
  msg += "\">\n";
  msg += "<br>";
  msg += "<br>\n";
  msg += "<label>Subscriber : ";
  msg += String(credit);
  msg += " credits</label>\n";
  msg += "<br>";
  msg += "<br>\n";
  msg += "<input type=\"radio\" name=\"action\" value=\"add1\" checked>Add 1 credit\n";
  msg += "<br>\n";
  msg += "<input type=\"radio\" name=\"action\" value=\"add5\">Add 5 credits\n";
  msg += "<br>\n";
  msg += "<input type=\"radio\" name=\"action\" value=\"add10\">Add 10 credits\n";
  msg += "<br>\n";
  msg += "<input type=\"radio\" name=\"action\" value=\"reset\">Reset subscriber\n";
  msg += "<br>\n";
  msg += "<input type=\"radio\" name=\"action\" value=\"deactivate\">Deactivate subscriber\n";
  msg += "<br>\n";
  msg += "<input type=\"radio\" name=\"action\" value=\"activate\">Reactivate subscriber\n";
  msg += "<br>";
  msg += "<br>\n";
  msg += "<label for=\"history\">History :</label>\n";
  msg += "<br>\n";
  msg += "<textarea id=\"history\" rows=\"10\" cols=\"40\" name=\"history\">";
  msg += history;
  msg += "</textarea>\n";
  msg += "<br>";
  msg += "<br>\n";
  msg += "<label for=\"status\">Status :</label>\n";
  msg += "<br>";
  msg += "<input type=\"text\" id=\"status\" name=\"status\" value=\"";
  msg += status;
  msg += "\" readonly style=\"background-color:";
  msg += statusColor;
  msg += ";color:white;\">\n";
  msg += "<br>";
  msg += "<br>\n";
  msg += "<input type=\"submit\" value=\"OK\">\n";
  msg += "</form>\n";
  msg += "</body>\n";
  msg += "</html>";
  request->send(200, "text/html", msg);
  Serial.print("response time: "); Serial.println(millis() - start);
}

#elif METHOD == USE_SPIFFS

String templateProcessor(const String& var)
{
  return getDictionaryValue(var.substring(strlen("PLACEHOLDER ")).c_str());
}

int SendSubscribersForm(AsyncWebServerRequest *request)
{
  unsigned long start = millis();
  request->send(SPIFFS, "/index.html", "text/html", false, templateProcessor);
  Serial.print("response time: "); Serial.println(millis() - start);
  return 0;
}

int SendNewSubscriberForm(AsyncWebServerRequest *request)
{
  unsigned long start = millis();
  request->send(SPIFFS, "/new.html", "text/html", false, templateProcessor);
  Serial.print("response time: "); Serial.println(millis() - start);
  return 0;
}

#endif
