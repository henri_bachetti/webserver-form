
#include "display.h"

int messageToDisplay;
int messageDisplayed;

display::display()
{
}

void display::begin()
{
  m_mainTask = xTaskGetCurrentTaskHandle();
  Serial.printf("main TASK: %X\n", m_mainTask);
}

void display::process(void)
{
  delay(10);
  if (messageToDisplay != messageDisplayed) {
    switch (messageToDisplay) {
      case NONE:
        clear();
        break;
      case ASK_FOR_CARD:
        displayAskForCard();
        break;
      case WELCOME:
        displayWelcome();
        break;
    }
  }
}

lcdDisplay::lcdDisplay() :
  display(),
  lcd(0x27, 20, 4)
{
}

void lcdDisplay::begin()
{
  display::begin();
  lcd.begin();
  lcd.backlight();
}

void lcdDisplay::clear()
{
  lcd.clear();
}

void lcdDisplay::displayWelcome(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Put your Card &");
    lcd.setCursor(0, 1);
    lcd.print("Press the button");
    messageDisplayed = WELCOME;
  }
  messageToDisplay = WELCOME;
}

void lcdDisplay::displayNoCard(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("No Card Detected");
    messageDisplayed = NO_CARD;
  }
  messageToDisplay = NO_CARD;
}

void lcdDisplay::displayUnknownCard(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Unknown Card");
    messageDisplayed = UNKNOWN_CARD;
  }
  messageToDisplay = UNKNOWN_CARD;
}

void lcdDisplay::displayNoCredit(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("No Credit");
    messageDisplayed = NO_CREDIT;
  }
  messageToDisplay = NO_CREDIT;
}

void lcdDisplay::displayInactiveCard(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Card is Deactivated");
    messageDisplayed = INACTIVE_CARD;
  }
  messageToDisplay = INACTIVE_CARD;
}

void lcdDisplay::displayEnter(int credit)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Please Enter");
    lcd.setCursor(0, 1);
    lcd.print("The Door is Open");
    lcd.setCursor(0, 2);
    lcd.print("Credit: ");
    lcd.print(credit);
    messageDisplayed = ENTER;
  }
  messageToDisplay = ENTER;
}

void lcdDisplay::displayAskForCard(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Put your card &");
    lcd.setCursor(0, 1);
    lcd.print("Press Read Media");
    messageDisplayed = ASK_FOR_CARD;
  }
  messageToDisplay = ASK_FOR_CARD;
}

oledDisplay::oledDisplay() :
  display(),
  oled(-1)
{
}

void oledDisplay::begin(void)
{
  display::begin();
  oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);
}

void oledDisplay::clear(void)
{
  oled.clearDisplay();
  oled.display();
}

void oledDisplay::displayWelcome(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    oled.clearDisplay();
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.setCursor(0, 0);
    oled.print("Put your Card &");
    oled.setCursor(0, 10);
    oled.print("Press the button");
    oled.display();
    messageDisplayed = WELCOME;
  }
  messageToDisplay = WELCOME;
}

void oledDisplay::displayNoCard(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    oled.clearDisplay();
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.setCursor(0, 0);
    oled.print("No Card Detected");
    oled.display();
    messageDisplayed = NO_CARD;
  }
  messageToDisplay = NO_CARD;
}

void oledDisplay::displayUnknownCard(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    oled.clearDisplay();
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.setCursor(0, 0);
    oled.print("Unknown Card");
    oled.display();
    messageDisplayed = UNKNOWN_CARD;
  }
  messageToDisplay = UNKNOWN_CARD;
}

void oledDisplay::displayNoCredit(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    oled.clearDisplay();
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.setCursor(0, 0);
    oled.print("No Credit");
    oled.display();
    messageDisplayed = NO_CREDIT;
  }
  messageToDisplay = NO_CREDIT;
}

void oledDisplay::displayInactiveCard(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    oled.clearDisplay();
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.setCursor(0, 0);
    oled.print("Card is Deactivated");
    oled.display();
    messageDisplayed = INACTIVE_CARD;
  }
  messageToDisplay = INACTIVE_CARD;
}

void oledDisplay::displayEnter(int credit)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    oled.clearDisplay();
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.setCursor(0, 0);
    oled.print("Please Enter");
    oled.setCursor(0, 10);
    oled.print("The Door is Open");
    oled.setCursor(0, 20);
    oled.print("Credit : ");
    oled.print(credit);
    oled.display();
    messageDisplayed = ENTER;
  }
  messageToDisplay = ENTER;
}

void oledDisplay::displayAskForCard(void)
{
  if (m_mainTask == xTaskGetCurrentTaskHandle()) {
    oled.clearDisplay();
    oled.setTextSize(1);
    oled.setTextColor(WHITE);
    oled.setCursor(0, 0);
    oled.print("Put your Card &");
    oled.setCursor(0, 10);
    oled.print("Press Read Media");
    oled.display();
    messageDisplayed = ASK_FOR_CARD;
  }
  messageToDisplay = ASK_FOR_CARD;
}
