import time
import unittest
from datetime import datetime
from urllib import request, parse
from html.parser import HTMLParser

# change to the ESP32 address
ipAddr = "192.168.1.18"
#change to match your name
owner = "Henri Bachetti"
owner_in_url = "Henri%20Bachetti"
#change to match the UID (0x4514d39a)
cardUID = "1158992794"
#change to match the UID (0x4514d39a)
cardHexUID = "0x4514d39a"

class MyHTMLParser(HTMLParser):

    def __init__(self):
        HTMLParser.__init__(self)
        self.state = 'idle'
        self.title = ''
        self.page_title = ''
        self.subscriber_name = ''
        self.subscriber_id = ''
        self.subscriber_credit = ''
        self.subscriber_history = ''
        self.status = ''
        self.status_color = ''

    def handle_starttag(self, tag, attrs):
#        print(tag, attrs)
        if (tag == 'title') :
            self.state = 'title'
        if (tag == 'h1') :
            self.state = 'page_title'
        if (tag == 'input') :
            attr_name = ''
            attr_value = ''
            attr_style = ''
            for attr in attrs:
                if attr[0] == 'name':
                    attr_name = attr[1]
                if attr[0] == 'value':
                    attr_value = attr[1]
                if attr[0] == 'style':
                    attr_style = attr[1]
            if attr_name == 'subscriber_name':
                self.subscriber_name = attr_value
            if attr_name == 'subscriber_id':
                self.subscriber_id = attr_value
            if attr_name == 'status':
                self.status = attr_value
                self.status_color = attr_style
        if (tag == 'label') :
            self.state = 'label'
        if (tag == 'textarea') :
            self.state = 'textarea'

    def handle_endtag(self, tag):
        self.state = 'idle'

    def handle_data(self, data):
#        print(data)
        if self.state == 'title':
            self.title = data
        if self.state == 'page_title':
            self.page_title = data
        if self.state == 'label':
            if data.startswith('Subscriber : '):
                self.subscriber_credit = data[13:]
        if self.state == 'textarea':
            self.subscriber_history = data

class BaseTest(unittest.TestCase):

    def setUp(self):
        self.parser = MyHTMLParser()

    def tearDown(self):
        self.clear_all()

    def get_ipaddr(self):
        global ipAddr
        return ipAddr

    def get_owner(self):
        global owner
        return owner

    def get_owner_in_url(self):
        global owner_in_url
        return owner_in_url

    def get_uid(self):
        global cardUID
        return cardUID

    def get_hex_uid(self):
        global cardHexUID
        return cardHexUID

    def clear_all(self):
        request.urlopen('http://%s/wipe.html' % self.get_ipaddr()).read()

    def fill_fs(self):
        html = request.urlopen('http://%s/freebytes.html' % self.get_ipaddr()).read()
        size = int(html.decode("utf-8").split(' ')[0])
        n = 0
        fsize = 100000
        while size > 4096:
            html = request.urlopen('http://%s/create.html?name=/tmp%d&size=%d' % (self.get_ipaddr(), n, fsize)).read()
            if html.decode("utf-8") == 'FATFS is almost full':
                if fsize == 10000:
                    break
                fsize = 10000
            html = request.urlopen('http://%s/freebytes.html' % self.get_ipaddr()).read()
            size = int(html.decode("utf-8").split(' ')[0])
            if size < 100000 :
                fsize = size - 4096
            n += 1
        return size

    def get_time(self) :
        dt = datetime.now()
        # don't send request at hh:mm:59
        if dt.second == 59:
            time.sleep(2)
        dt = datetime.now()
        s = dt.strftime('%d/%m/%Y %H:%M')
        return s

    def wait_minute_00(self) :
        dt = datetime.now()
        if dt.second < 30:
            return
        print("wait %s seconds please" % (60 - dt.second))
        time.sleep(60 - dt.second)

class Utilities(unittest.TestCase):

    def setUp(self):
        self.parser = MyHTMLParser()

    def get_ipaddr(self):
        global ipAddr
        return ipAddr

    def get_owner(self):
        global owner
        return owner

    def get_owner_in_url(self):
        global owner_in_url
        return owner_in_url

    def get_uid(self):
        global cardUID
        return cardUID

    def get_time(self) :
        dt = datetime.now()
        # don't send request at hh:mm:59
        if dt.second == 59:
            time.sleep(2)
        dt = datetime.now()
        s = dt.strftime('%d/%m/%Y %H:%M')
        return s

    def test_get_free_ram(self):
        '''
        get free heap
        '''
        html = request.urlopen('http://%s/freeram.html' % self.get_ipaddr()).read()
        print ("Free RAM: %s" % html.decode("utf-8"))

        html = request.urlopen('http://%s/largestblock.html' % self.get_ipaddr()).read()
        print ("Largest block: %s" % html.decode("utf-8"))

    def test_heap(self):
        '''
        heap walk
        '''
        html = request.urlopen('http://%s/heapdump.html' % self.get_ipaddr()).read()
        print ("Free RAM: %s" % html.decode("utf-8"))

    def test_clear_all(self):
        '''
        clear all data
        '''
        request.urlopen('http://%s/wipe.html' % self.get_ipaddr())

    def test_add_subscriber(self):
        '''
        add a subscriber without cleaning
        '''
        request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid()))

    def test_500(self):
        '''
        add a lot of credit without cleaning
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        s = self.get_time()
        history = s + ' : subscriber created\\n'
        i = 500
        credits = 1
        while i:
            data = parse.urlencode({'action': 'add1'}).encode("ascii")
            html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
            self.parser.feed(str(html))
            self.assertEqual(self.parser.title, 'Subscribers')
            self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
            self.assertEqual(self.parser.subscriber_name, self.get_owner())
            self.assertEqual(self.parser.subscriber_id, '4514d39a')
            self.assertEqual(self.parser.subscriber_credit, '%d credits' % credits)
            self.assertEqual(self.parser.status, '1 credit added')
            self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
            credits += 1
            i -= 1

class AutomatedTest(BaseTest):

    def test_first_launch(self):
        '''
        first launch
        '''
        html = request.urlopen('http://%s' % self.get_ipaddr()).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, 'Unknown')
        self.assertEqual(self.parser.subscriber_id, '0')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        self.assertEqual(self.parser.subscriber_history, '\\n')
        self.assertEqual(self.parser.status, 'No Subscribers registered')
        self.assertEqual(self.parser.status_color, 'background-color:red;color:white;')

    def test_add_subscriber(self):
        '''
        add a subscriber
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber is OK')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')

    def test_add_credit(self):
        '''
        add some credits
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        data = parse.urlencode({'action': 'add1'}).encode("ascii")
        html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '1 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : +1 credit\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, '1 credit added')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
        data = parse.urlencode({'action': 'add5'}).encode('ascii')
        html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '6 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : +5 credits\\n' + s + ' : +1 credit\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, '5 credits added')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
        data = parse.urlencode({'action': 'add10'}).encode('ascii')
        html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '16 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history,  s + ' : +10 credits\\n' + s + ' : +5 credits\\n' + s + ' : +1 credit\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, '10 credits added')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')

    def test_reset(self):
        '''
        reset credit
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        data = parse.urlencode({'action': 'add1'}).encode("ascii")
        html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '1 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : +1 credit\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, '1 credit added')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
        data = parse.urlencode({'action': 'reset'}).encode("ascii")
        html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : reset credits\\n' + s + ' : +1 credit\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Credits = 0')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')

    def test_deactivate(self):
        '''
        deactivate subsciber
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        data = parse.urlencode({'action': 'deactivate'}).encode("ascii")
        html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : deactivated\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber is DEACTIVATED !!!')
        self.assertEqual(self.parser.status_color, 'background-color:red;color:white;')

    def test_reactivate(self):
        '''
        reactivate subsciber
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        data = parse.urlencode({'action': 'activate'}).encode("ascii")
        html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : activated\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber is OK')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')

    def test_search(self):
        '''
        search by name and ID
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        html = request.urlopen('http://%s/?search_name=%s' % (self.get_ipaddr(), self.get_owner_in_url())).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber is OK')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
        html = request.urlopen('http://%s/?search_id=%s' % (self.get_ipaddr(), self.get_hex_uid())).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber is OK')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')

    def test_search_not_found(self):
        '''
        search by name and ID (not found)
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        html = request.urlopen('http://%s/?search_name=%s' % (self.get_ipaddr(), 'foo')).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.subscriber_name, 'Unknown')
        self.assertEqual(self.parser.subscriber_id, '0')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, '\\n')
        self.assertEqual(self.parser.status, 'Subscriber NOT FOUND !!!')
        self.assertEqual(self.parser.status_color, 'background-color:red;color:white;')
        html = request.urlopen('http://%s/?search_id=%s' % (self.get_ipaddr(), '12345678')).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.subscriber_name, 'Unknown')
        self.assertEqual(self.parser.subscriber_id, '0')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, '\\n')
        self.assertEqual(self.parser.status, 'Subscriber NOT FOUND !!!')
        self.assertEqual(self.parser.status_color, 'background-color:red;color:white;')

    def test_subscriber_exists(self):
        '''
        add a subscriber (already exists)
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber is OK')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber EXISTS !!!')
        self.assertEqual(self.parser.status_color, 'background-color:red;color:white;')

class SpecialTest(BaseTest):

    def test_fatfs_full(self):
        '''
        fill FATFS and add credits until the end
        '''
        self.fill_fs()
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        s = self.get_time()
        history = s + ' : subscriber created\\n'
        while 1:
            s = self.get_time()
            html = request.urlopen('http://%s/freebytes.html' % self.get_ipaddr()).read()
            size = int(html.decode("utf-8").split(' ')[0])
            html = request.urlopen('http://%s/?subscriber=1' % self.get_ipaddr()).read()
            data = parse.urlencode({'action': 'add1'}).encode('ascii')
            html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
            self.parser.feed(str(html))
            history = s + ' : +1 credit\\n' + history
            self.assertEqual(self.parser.subscriber_history, history+'\\n')
            try :
                self.assertEqual(self.parser.status, '1 credit added')
                self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
            except:
                self.assertEqual(self.parser.status, 'FAT volume is FULL')
                self.assertEqual(self.parser.status_color, 'background-color:red;color:white;')
                break

    def test_endurance(self):
        '''
        add a lot of credit
        '''
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        s = self.get_time()
        history = s + ' : subscriber created\\n'
        i = 5000
        credits = 1
        while i:
            data = parse.urlencode({'action': 'add1'}).encode("ascii")
            html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
            self.parser.feed(str(html))
            self.assertEqual(self.parser.title, 'Subscribers')
            self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
            self.assertEqual(self.parser.subscriber_name, self.get_owner())
            self.assertEqual(self.parser.subscriber_id, '4514d39a')
            self.assertEqual(self.parser.subscriber_credit, '%d credits' % credits)
            self.assertEqual(self.parser.status, '1 credit added')
            self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
            credits += 1
            i -= 1


class InteractiveTest(BaseTest):
    def test_new_subscriber_page(self):
        '''
        new subscriber page
        '''
        html = request.urlopen('http://%s/new.html' % self.get_ipaddr()).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'ADD A NEW SUBSCRIBER')

    def test_add_subscriber(self):
        '''
        add a subscriber interactively
        '''
        self.wait_minute_00()
        print('Click on the "Add a new subscriber" button')
        print('Enter "Henri Bachetti" in the "Subscriber name" field')
        print('present the 4514d39a card and click on the "Read media" button')
        input('Then press RETURN')
        html = request.urlopen('http://%s/' % self.get_ipaddr()).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '0 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber is OK')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')

    def test_subscriber_consume(self):
        '''
        consume 1 credit interactively
        must have a push-button on pin 15
        '''
        self.wait_minute_00()
        html = request.urlopen('http://%s/?action=add_subscriber&name=%s&uid=%s' % (self.get_ipaddr(), self.get_owner_in_url(), self.get_uid())).read()
        data = parse.urlencode({'action': 'add5'}).encode("ascii")
        html = request.urlopen('http://%s/' % self.get_ipaddr(), data=data).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '5 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : +5 credits\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, '5 credits added')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')
        print('present the 4514d39a card and press the push-button')
        input('Then press RETURN')
        html = request.urlopen('http://%s/' % self.get_ipaddr()).read()
        self.parser.feed(str(html))
        self.assertEqual(self.parser.title, 'Subscribers')
        self.assertEqual(self.parser.page_title, 'SUBSCRIBERS')
        self.assertEqual(self.parser.subscriber_name, self.get_owner())
        self.assertEqual(self.parser.subscriber_id, '4514d39a')
        self.assertEqual(self.parser.subscriber_credit, '4 credits')
        s = self.get_time()
        self.assertEqual(self.parser.subscriber_history, s + ' : -1 credit\\n' + s + ' : +5 credits\\n' + s + ' : subscriber created\\n\\n')
        self.assertEqual(self.parser.status, 'Subscriber is OK')
        self.assertEqual(self.parser.status_color, 'background-color:green;color:white;')

if __name__ == "__main__":
    unittest.main()
