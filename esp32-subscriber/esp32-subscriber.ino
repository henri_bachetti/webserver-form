
#define NODISPLAY     0
#define USE_LCD       1
#define USE_SSD1306   2

#define SCREEN USE_LCD

#include <WiFi.h>
#include <SPIFFS.h>
#include <FFat.h>
#include <ESPmDNS.h>
#include <time.h>

#include "subscriber.h"
#include "template.h"
#include "pages.h"
#include "display.h"
#include "version.h"

#if SCREEN == USE_LCD
lcdDisplay display;
#elif SCREEN == USE_SSD1306
oledDisplay display;
#endif

#define LED_BUILTIN   2
#define BUTTON        15
#define DISPENSER     LED_BUILTIN

const char* ssid = "........";
const char* password = "........";

const char* ntpServer = "pool.ntp.org";

AsyncWebServer server(80);

void displayArgs(const char *name, AsyncWebServerRequest *request)
{
  Serial.printf("### %s : ", name);
  for (int i = 0 ; i < request->args() ; i++) {
    Serial.printf("%s=%s ", request->argName(i).c_str(), request->arg(i).c_str());
  }
  Serial.println();
}

void handleRoot(AsyncWebServerRequest *request)
{
  char timeString[20];
  struct tm timeinfo;
  const char *status = "Subscriber is OK";
  const char *statusColor = "green";
  int subscriberIndex = 0;
  const char *subscriberName;
  String action;

  displayArgs("handleRoot", request);
  display.displayWelcome();
  if (Subscriber::getTotalSubscribers() > 0) {
    subscriberIndex = 1;
  }
  Subscriber *subscriber = Subscriber::fromIndex(subscriberIndex);
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return;
  }
  else {
    strftime(timeString, sizeof(timeString), "%d/%m/%Y %H:%M", &timeinfo);
  }
  if (!strcmp(request->methodToString(), "GET")) {
    /*
       GET request (2 URLs) :
       /?subscriber=value
       /?search_id=value
       /?search_name=value
       /?action=read_media
       /?action=new_subscriber&name=value
    */
    Serial.println("GET method");
    if (request->hasParam("action", false)) {
      action = request->arg("action");
      if (action == "read_media") {
        subscriberIndex = Subscriber::readMedia();
      }
      else if (action == "new_subscriber") {
        subscriberIndex = Subscriber::fromName(request->arg("name").c_str());
        if (subscriberIndex == ERR_NOTFOUND) {
          subscriberIndex = Subscriber::newSubscriber(request->arg("name").c_str());
          Serial.printf("new subscriber %d\n", subscriberIndex);
        }
        else {
          status = "Subscriber EXISTS !!!";
          statusColor = "red";
        }
        display.displayWelcome();
      }
      else if (action == "add_subscriber") {
        uint32_t uid;
        if (request->hasParam("uid", false)) {
          uid = request->arg("uid").toInt();
        }
        subscriberIndex = Subscriber::fromName(request->arg("name").c_str());
        if (subscriberIndex == ERR_NOTFOUND) {
          subscriberIndex = Subscriber::addSubscriber(request->arg("name").c_str(), uid);
        }
        else {
          status = "Subscriber EXISTS !!!";
          statusColor = "red";
        }
      }
    }
    if (request->hasParam("subscriber", false)) {
      subscriberIndex = request->arg("subscriber").toInt();
    }
    if (request->hasParam("search_id", false)) {
      const char *s = request->arg("search_id").c_str();
      uint32_t id;
      sscanf(s, "%lx", &id);
      subscriberIndex = Subscriber::fromId(id);
      Serial.printf("### subscriber %d\n", subscriberIndex);
    }
    if (request->hasParam("search_name", false)) {
      subscriberIndex = Subscriber::fromName(request->arg("search_name").c_str());
      Serial.printf("### subscriber %d\n", subscriberIndex);
    }
    switch (subscriberIndex) {
      case 0:
        if (Subscriber::getTotalSubscribers() == 0) {
          status = "No Subscribers registered";
        }
        else {
          status = "Subscriber is UNKNOWN !!!";
        }
        break;
      case ERR_NOCARD:
        status = "NO CARD !!!";
        break;
      case ERR_READCARD:
        status = "Error reading card !!!";
        break;
      case ERR_CARDTYPE:
        status = "Card in not MIFARE !!!";
        break;
      case ERR_NOTFOUND:
        status = "Subscriber NOT FOUND !!!";
        break;
      default:
        break;
    }
    if (subscriberIndex <= 0) {
      subscriberIndex = 0;
      statusColor = "red";
    }
    subscriber = Subscriber::fromIndex(subscriberIndex);
  }
  else {
    /*
       POST request (1 URL) :
       /?action=value&...
    */
    Serial.println("POST method");
    if (request->hasParam("subscriber", true)) {
      subscriberIndex = request->getParam("subscriber", true)->value().toInt();
    }
    subscriber = Subscriber::fromIndex(subscriberIndex);
    if (subscriber == 0) {
      status = "Subscriber not found !!!";
      statusColor = "red";
    }
    if (request->hasParam("action", true)) {
      action = request->getParam("action", true)->value();
      int error = NO_ERROR;
      if (subscriber->isActive()) {
        if (action == "add1") {
          Serial.println("add 1 credit");
          status = "1 credit added";
          if ((error = subscriber->addCredit(1)) == NO_ERROR) {
            error = subscriber->addToHistory("+1 credit");
          }
        }
        else if (action == "add5") {
          Serial.println("add 5 credits");
          status = "5 credits added";
          if ((error = subscriber->addCredit(5))  == NO_ERROR) {
            error = subscriber->addToHistory("+5 credits");
          }
        }
        else if (action == "add10") {
          Serial.println("add 10 credits");
          status = "10 credits added";
          if ((error = subscriber->addCredit(10)) == NO_ERROR) {
            error = subscriber->addToHistory("+10 credits");
          }
        }
        else if (action == "reset") {
          Serial.println("reset subscriber");
          status = "Credits = 0";
          if ((error = subscriber->resetCredit()) == NO_ERROR) {
            error = subscriber->addToHistory("reset credits");
          }
        }
        else if (action == "deactivate") {
          Serial.println("deactivate subscriber");
          if ((error = subscriber->deactivate()) == NO_ERROR) {
            error = subscriber->addToHistory("deactivated");
          }
        }
      }
      if (action == "activate") {
        Serial.println("reactivate subscriber");
        if ((error = subscriber->activate()) == NO_ERROR) {
          error = subscriber->addToHistory("activated");
        }
      }
      switch (error) {
        case ERR_OPEN:
          status = "Cannot open file";
          break;
        case ERR_WRITE:
          status = "Cannot write to file";
          break;
        case ERR_FATFULL:
          status = "FAT volume is FULL";
          break;
        case ERR_GETTIME:
          status = "Cannot get time";
          break;
        default:
          break;
      }
      if (error != NO_ERROR) {
        statusColor = "red";
      }
    }
  }
  if (subscriberIndex > 0 && !subscriber->isActive()) {
    status = "Subscriber is DEACTIVATED !!!";
    statusColor = "red";
  }
  String subscriberHexId = String(subscriber->getId(), HEX);
  subscriberName = subscriber->getName();
  Serial.print("subscriber index: ");  Serial.println(subscriberIndex);
  Serial.print("subscriber ID: "); Serial.println(subscriberHexId);
  Serial.print("subscriber Name: "); Serial.println(subscriberName);
#if METHOD == USE_SPIFFS
  clearDictionary();
  addToDictionary("DATE", timeString);
  addToDictionary("SUBSCRIBER", subscriberIndex);
  addToDictionary("NSUBSCRIBERS", Subscriber::getTotalSubscribers());
  addToDictionary("SUBSCRIBER_ID", subscriberHexId.c_str());
  addToDictionary("SUBSCRIBER_NAME", subscriberName);
  addToDictionary("SUBSCRIBER_CREDIT", subscriber->getCredit());
  addToDictionary("SUBSCRIBER_HISTORY", subscriber->getHistory().c_str());
  addToDictionary("STATUS", status);
  addToDictionary("STATUS_COLOR", statusColor);
  addToDictionary("TITLE", title);
  addToDictionary("AUTHOR", author);
  addToDictionary("WEBSITE", website);
  addToDictionary("VERSION", version);
  addToDictionary("FREERAM", ESP.getFreeHeap());
  SendSubscribersForm(request);
#else
  SendSubscribersForm(request, timeString, subscriberIndex, Subscriber::getTotalSubscribers(),
                      subscriberId, subscriberName, subscriber->getCredit(), subscriber->getHistory().c_str(), status, statusColor);
#endif
}

void handleNotFound(AsyncWebServerRequest * request) {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += request->url();
  message += "\nMethod: ";
  message += request->methodToString();
  message += "\nArguments: ";
  message += request->args();
  message += "\n";
  for (uint8_t i = 0; i < request->args(); i++) {
    message += " " + request->argName(i) + ": " + request->arg(i) + "\n";
  }
  request->send(404, "text/plain", message);
}

void setup(void)
{
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  Serial.print("Connecting to "); Serial.println(ssid);
  WiFi.begin(ssid, password);
  Serial.println("");
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(DISPENSER, OUTPUT);
  display.begin();
  display.displayWelcome();
  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }
  if (!FFat.begin(true)) {
    Serial.println("An Error has occurred while mounting FATFS");
    return;
  }
  Subscriber::begin();
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to "); Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp32")) {
    Serial.println("MDNS responder started");
  }

  configTzTime("CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00", ntpServer);

  server.on("/", handleRoot);

  server.on("/new.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    displayArgs("handleNew", request);
#if METHOD == USE_SPIFFS
    struct tm timeinfo;
    char timeString[20];

    display.displayAskForCard();
    if (!getLocalTime(&timeinfo)) {
      Serial.println("Failed to obtain time");
      return;
    }
    else {
      strftime(timeString, sizeof(timeString), "%d/%m/%Y %H:%M", &timeinfo);
    }
    clearDictionary();
    addToDictionary("DATE", timeString);
    SendNewSubscriberForm(request);
#else
    handleNotFound();
#endif
  });
  server.on("/explore.html", HTTP_GET, [](AsyncWebServerRequest * request) {
#if METHOD == USE_SPIFFS
    String name = "/";
    int fromLine = 0, line = 0;
    String message = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Subscribers</title></head><body>";

    if (request->hasParam("delete", false)) {
      name = request->arg("delete");
      if (FFat.remove(name)) {
        Serial.printf("%s deleted\n", name.c_str());
      } else {
        Serial.printf("%s delete failed\n", name.c_str());
      }
      name = "/";
    }
    if (request->hasParam("name", false)) {
      name = request->arg("name");
    }
    if (name == "/") {
      File root = FFat.open("/");
      if (!root) {
        Serial.println("- failed to open directory");
        return;
      }
      if (!root.isDirectory()) {
        Serial.println(" - not a directory");
        return;
      }
      message += "<h1>DIRECTORY LISTING</h1>\n";
      File file = root.openNextFile();
      while (file) {
        message += "<button onclick=\"window.location.href='/explore.html?name=";
        message += file.name();
        message += "'\">Read</button> \n";
        message += "<button onclick=\"window.location.href='/explore.html?delete=";
        message += file.name();
        message += "'\">Delete</button> \n";
        message += file.name();
        message += " : ";
        message += file.size();
        message += " bytes<br>\n";
        file = root.openNextFile();
      }
    }
    else {
      if (request->hasParam("line", false)) {
        fromLine = request->arg("line").toInt();
      }
      File f = FFat.open(name, "r");
      if (!f) {
        Serial.println("Error opening file");
        return;
      }
      message += "<h1>";
      message += name;
      message += "</h1>";
      while (f.available()) {
        if (line >= fromLine && line < fromLine + 200) {
          message += line + 1;
          message += " ";
          message += f.readStringUntil('\n');
          message += "<br>\n";
        }
        else {
          f.readStringUntil('\n');
        }
        line++;
      }
      f.close();
    }
    if (line > 200) {
      message += "<a href=\"/explore.html?name=" + name + "&line=";
      message += fromLine + 200;
      message += "\" >More ...</a>";
    }
    message += "</body></html>";
    request->send(200, "text/html", message);
#else
    handleNotFound();
#endif
  });

  server.on("/wipe.html", HTTP_GET, [](AsyncWebServerRequest * request) {
#if METHOD == USE_SPIFFS
    String message = "<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Subscribers</title></head><body>";

    File root = FFat.open("/");
    if (!root) {
      Serial.println("- failed to open directory");
      return;
    }
    if (!root.isDirectory()) {
      Serial.println(" - not a directory");
      return;
    }
    message += "<h1>DIRECTORY has been WIPED</h1>\n";
    File file = root.openNextFile();
    while (file) {
      message += file.name();
      message += "<br>\n";
      if (FFat.remove(file.name())) {
        Serial.printf("%s deleted\n", file.name());
      } else {
        Serial.printf("%s delete failed\n", file.name());
      }
      file = root.openNextFile();
    }
    Subscriber::clearAll();
    message += "</body></html>";
    request->send(200, "text/html", message);
#else
    handleNotFound();
#endif
  });

  server.on("/create.html", HTTP_GET, [](AsyncWebServerRequest * request) {
#if METHOD == USE_SPIFFS
    uint8_t buf[1000];
    String name;
    ulong fsize;

    if (request->hasParam("name", false)) {
      name = request->arg("name");
    }
    if (request->hasParam("size", false)) {
      fsize = request->arg("size").toInt();
    }
    ulong remaining = FFat.freeBytes();
    if (remaining < fsize + CONFIG_WL_SECTOR_SIZE) {
      request->send(200, "text/plain", "FATFS is almost full");
      return;
    }
    Serial.printf("create %s %ld\n", name.c_str(), fsize);
    memset(buf, 'a', 1000);
    File f = FFat.open(name, FILE_WRITE);
    if (!f) {
      request->send(200, "text/plain", "cannot create file");
      return;
    }
    ulong size = fsize;
    while (size > 0) {
      int len = size;
      if (len > 1000) {
        len = 1000;
      }
      remaining = FFat.freeBytes();
      if (remaining < CONFIG_WL_SECTOR_SIZE) {
        f.close();
        Serial.printf("stop. remaining %ld bytes\n", remaining);
        request->send(200, "text/plain", "file truncated");
        return;
      }
      f.write(buf, len);
      size -= len;
    }
    f.close();
    Serial.printf("remaining %ld bytes\n", remaining);
    request->send(200, "text/plain", "file created");
#else
    handleNotFound();
#endif
  });

  server.on("/freebytes.html", HTTP_GET, [](AsyncWebServerRequest * request) {
#if METHOD == USE_SPIFFS
    String message(FFat.freeBytes());
    message  += " remaining";
    request->send(200, "text/plain", message);
#else
    handleNotFound();
#endif
  });

  server.on("/freeram.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    String message(ESP.getFreeHeap());
    request->send(200, "text/plain", message);
  });

  server.on("/largestblock.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    String message(heap_caps_get_largest_free_block(MALLOC_CAP_8BIT));
    request->send(200, "text/plain", message);
  });

  server.on("/heapdump.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    heap_caps_dump(MALLOC_CAP_8BIT);
    String message("OK");
    request->send(200, "text/plain", message);
  });

  server.on("/lock.jpg", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/lock.jpg", "image/jpg");
  });  server.onNotFound(handleNotFound);

  server.on("/favicon.ico", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/favicon.jpg", "image/jpg");
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void flash(int n)
{
  for (int i = 0 ; i < n ; i++) {
    digitalWrite(DISPENSER, HIGH);
    delay(100);
    digitalWrite(DISPENSER, LOW);
    delay(200);
  }
}

void loop(void)
{
  static unsigned long start;

  if (digitalRead(BUTTON) == LOW) {
    delay(1000);
    int subscriberIndex = Subscriber::readMedia();
    if (subscriberIndex == -1) {
      display.displayNoCard();
      delay(3000);
      display.displayWelcome();
    }
    else if (subscriberIndex == 0) {
      display.displayUnknownCard();
      flash(1);
      delay(3000);
      display.displayWelcome();
    }
    else {
      Subscriber *subscriber = Subscriber::fromIndex(subscriberIndex);
      if (!subscriber->isActive()) {
        display.displayInactiveCard();
        flash(3);
        delay(2000);
        display.displayWelcome();
      }
      else if (subscriber->getCredit() == 0) {
        display.displayNoCredit();
        flash(6);
        display.displayWelcome();
      }
      else {
        digitalWrite(DISPENSER, HIGH);
        start = millis();
        subscriber->addCredit(-1);
        display.displayEnter(subscriber->getCredit());
        subscriber->addToHistory("-1 credit");
      }
    }
  }
  display.process();
  if (start != 0 && millis() - start > 10000) {
    display.displayWelcome();
    digitalWrite(DISPENSER, LOW);
    start = 0;
  }
}
