
#ifndef __PAGES_H__
#define __PAGES_H__

#include <Arduino.h>
#include <ESPAsyncWebServer.h>

#define USE_SPRINTF       1
#define USE_STRING        2
#define USE_SPIFFS        3
#define METHOD            USE_SPIFFS

#if METHOD == USE_SPIFFS
int SendSubscribersForm(AsyncWebServerRequest *request);
int SendNewSubscriberForm(AsyncWebServerRequest *request);
#else
int SendSubscribersForm(AsyncWebServerRequest *request, const char *timeString, int subscriberIndex, int totalSubscribers, int subscriberId, const char *subscriberName,
                         int credit, const char *history, const char *status, const char *statusColor);
#endif

#endif
