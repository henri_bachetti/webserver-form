
#include <Arduino.h>
#include <time.h>
#include <list>
#include <SPI.h>
#include <FFat.h>
#include <FS.h>
#include <MFRC522.h>

#include "subscriber.h"

#define SS_PIN 4
#define RST_PIN 5

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class

MFRC522::MIFARE_Key key;

// Init array that will store new NUID
byte nuidPICC[4];

std::list<Subscriber> subscribersList;

int Subscriber::s_index;

int Subscriber::getTotalSubscribers(void)
{
  // subscriber 0 = unknown
  return subscribersList.size() - 1;
}

int Subscriber::readRFID(uint32_t *uid)
{
  if ( ! rfid.PICC_IsNewCardPresent()) {
    Serial.println(F("No card ..."));
    return ERR_NOCARD;
  }
  if ( ! rfid.PICC_ReadCardSerial()) {
    Serial.println(F("Error reading card ..."));
    return ERR_READCARD;
  }
  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return ERR_CARDTYPE;
  }
  if (rfid.uid.uidByte[0] != nuidPICC[0] ||
      rfid.uid.uidByte[1] != nuidPICC[1] ||
      rfid.uid.uidByte[2] != nuidPICC[2] ||
      rfid.uid.uidByte[3] != nuidPICC[3] ) {
    Serial.println(F("A new card has been detected."));
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
    Serial.print(F("NUID : "));
    Serial.println(*(uint32_t *)rfid.uid.uidByte, HEX);
  }
  else {
    Serial.println(F("Card read previously."));
  }
  rfid.PICC_HaltA();
  rfid.PCD_StopCrypto1();
  *uid = *(uint32_t *)rfid.uid.uidByte;
  return 0;
}

int Subscriber::readMedia(void)
{
  std::list<Subscriber>::iterator it;
  uint32_t uid;
  int status;

  if ((status = readRFID(&uid)) < 0) {
    return status;
  }
  for (it = subscribersList.begin() ; it != subscribersList.end() ; ++it) {
    if (uid == it->getId()) {
      return it->m_index;
    }
  }
  return 0;
}

int Subscriber::addSubscriber(const char*name, uint32_t uid)
{
  char buf[80];
  int status;

  File f = FFat.open(SUBSCRIBERS_FILE, FILE_APPEND);
  if (!f) {
    Serial.printf("%s: cannot open (a)\n", SUBSCRIBERS_FILE);
    return 0;
  }
  Serial.printf("%s:%lx,1,0\n", name, uid);
  int len = snprintf(buf, 80, "%s:%ld, 1, 0\n", name, uid);
  ulong remaining = FFat.freeBytes();
  if (remaining < len) {
    Serial.printf("%s: FAT volume is FULL(%l)\n", SUBSCRIBERS_FILE, remaining);
    return ERR_FATFULL;
  }
  if (f.print(buf) != len) {
    return ERR_WRITE;
  }
  f.close();
  Subscriber subscriber(uid, name, true, 0, "");
  subscriber.addToHistory("subscriber created");
  int index = subscribersList.size();
  subscribersList.push_back(subscriber);
  return index;
}

int Subscriber::newSubscriber(const char*name)
{
  char buf[80];
  uint32_t uid;
  int status;

  if ((status = readRFID(&uid)) < 0) {
    return status;
  }
  return addSubscriber(name, uid);
}

int Subscriber::fromId(uint32_t id)
{
  std::list<Subscriber>::iterator it;

  for (it = subscribersList.begin() ; it != subscribersList.end() ; ++it) {
    if (it->m_id == id) {
      Serial.printf("fromId index %d found\n", it->m_index);
      return it->m_index;
    }
  }
  Serial.printf("fromId %lx not found\n", id);
  return ERR_NOTFOUND;
}

int Subscriber::fromName(const char *name)
{
  std::list<Subscriber>::iterator it;

  for (it = subscribersList.begin() ; it != subscribersList.end() ; ++it) {
    if (it->m_name == name) {
      Serial.printf("fromName index %d found\n", it->m_index);
      return it->m_index;
    }
  }
  Serial.printf("fromName %s not found\n", name);
  return ERR_NOTFOUND;
}

Subscriber *Subscriber::fromIndex(int index)
{
  std::list<Subscriber>::iterator it;

  for (it = subscribersList.begin() ; it != subscribersList.end() ; ++it) {
    if (it->m_index == index) {
      Serial.printf("subscriber index %d found\n", index);
      return &*it;
    }
  }
  Serial.printf("subscriber index %d not found\n", index);
  return &*subscribersList.begin();
}

int Subscriber::saveToFat(void)
{
  std::list<Subscriber>::iterator it;
  char buf[80];

  File f = FFat.open(SUBSCRIBERS_FILE, FILE_WRITE);
  if (!f) {
    Serial.printf("%s: cannot open (w)\n", SUBSCRIBERS_FILE);
    return ERR_OPEN;
  }
  for (it = subscribersList.begin() ; it != subscribersList.end() ; ++it) {
    int len = snprintf(buf, 80, "%s:%ld,%d,%d\n", it->m_name.c_str(), it->m_id, it->m_active, it->m_credit);
    ulong remaining = FFat.freeBytes();
    if (remaining < len) {
      Serial.printf("%s: FAT volume is FULL(%l)\n", SUBSCRIBERS_FILE, remaining);
      return ERR_FATFULL;
    }
    if (f.print(buf) != len) {
      Serial.printf("%s: unable to write\n", SUBSCRIBERS_FILE);
      return ERR_WRITE;
    }
    Serial.print(buf);
  }
  f.close();
  Serial.printf("%d lines saved\n", subscribersList.size() - 1);
  return 0;
}

Subscriber::Subscriber(uint32_t id, const char *name, bool active, unsigned long credit, const char *history) :
  m_id(id),
  m_name(name),
  m_active(active),
  m_credit(credit),
  m_history(history)
{
  m_index = s_index++;
}

int Subscriber::activate(void)
{
  m_active = true;
  return saveToFat();
}

int Subscriber::deactivate(void)
{
  m_active = false;
  return saveToFat();
}

int Subscriber::addCredit(int n)
{
  m_credit += n;
  return saveToFat();
}

int Subscriber::resetCredit(void)
{
  m_credit = 0;
  return saveToFat();
}

int Subscriber::addToHistory(String s)
{
  struct tm timeinfo;

  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return ERR_GETTIME;
  }
  char actionTimeString[20];
  strftime(actionTimeString, sizeof(actionTimeString), "%d/%m/%Y %H:%M", &timeinfo);
  String history = actionTimeString;
  history += " : ";
  history += s;
  history += '\n';
  history += m_history;
  int lines = 0;
  // limit history to 20 lines
  for (int i = 0 ; i < history.length() ; i++) {
    if (history[i] == '\n') {
      lines++;
      if (lines == 20) {
        history = history.substring(0, i);
        break;
      }
    }
  }
  m_history = history;

  String fname("/");
  fname += m_name;
  int len = s.length() + strlen(actionTimeString) + 3;
  File f = FFat.open(fname, FILE_APPEND);
  if (!f) {
    Serial.printf("%s: cannot open (w)\n", fname.c_str());
    return ERR_OPEN;
  }
  Serial.printf("%s : %s\n", actionTimeString, s.c_str());
  ulong remaining = FFat.freeBytes();
  if (remaining < len) {
    Serial.printf("%s: FAT volume is FULL(%l)\n", fname.c_str(), remaining);
    return ERR_FATFULL;
  }
  if (f.print(actionTimeString) != strlen(actionTimeString)) {
    Serial.printf("%s: cannot write actionTimeString\n", fname.c_str());
    return ERR_WRITE;
  }
  if (f.print(" : ") != 3) {
    Serial.printf("%s: cannot write :\n", fname.c_str());
    return ERR_WRITE;
  }
  if (f.print(s + '\n') != s.length() + 1) {
    Serial.printf("%s: cannot write s\n", fname.c_str());
    return ERR_WRITE;
  }
  f.close();
  return 0;
}

void Subscriber::clearAll(void)
{
  Serial.println("clear all data");
  File f = FFat.open(SUBSCRIBERS_FILE, "w");
  if (!f) {
    Serial.printf("%s: cannot open (w)\n", SUBSCRIBERS_FILE);
    return;
  }
  f.println("Unknown:0,0,0");
  f.close();
  subscribersList.clear();
  s_index = 0;
  Subscriber subscriber(0, "Unknown", false, 0, "");
  subscribersList.push_back(subscriber);
  Serial.printf("size = %d\n", subscribersList.size());
}

void Subscriber::begin(void)
{
  SPI.begin(); // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
  if (FFat.exists(SUBSCRIBERS_FILE)) {
    Serial.println("credit file exists");
    File f = FFat.open(SUBSCRIBERS_FILE, FILE_READ);
    if (!f) {
      Serial.printf("%s: cannot open\n", SUBSCRIBERS_FILE);
      return;
    }
    while (f.available()) {
      String name = f.readStringUntil(':');
      uint32_t uid = f.readStringUntil(',').toInt();
      int active = f.readStringUntil(',').toInt();
      int credit = f.readStringUntil('\n').toInt();
      Serial.printf("%s:%lx,%d,%d\n", name.c_str(), uid, active, credit);
      Subscriber subscriber(uid, name.c_str(), active, credit, "");
      if (name != "Unknown") {
        String fname("/");
        fname += name;
        File h = FFat.open(fname, FILE_READ);
        if (!h) {
          Serial.printf("%s: cannot open (w)\n", fname);
          continue;
        }
        String history = "";
        int lines = 0;
        while (h.available()) {
          h.readStringUntil('\n');
          lines++;
        }
        h.seek(0);
        int line = 0;
        while (h.available()) {
          if (lines <= 20 || line >= lines - 20) {
            // read history and order it backwards (20 lines max)
            history = h.readStringUntil('\n') + '\n' + history;
          }
          else {
            h.readStringUntil('\n');
          }
          line += 1;
        }
        subscriber.m_history = history;
        h.close();
        Serial.println("history loaded");
      }
      subscribersList.push_back(subscriber);
    }
    f.close();
  }
  else {
    clearAll();
  }
}
