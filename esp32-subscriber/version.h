
#ifndef __VERSION_H__
#define __VERSION_H__

extern const char *title;
extern const char *author;
extern const char *website;
extern const char *version;

#endif
