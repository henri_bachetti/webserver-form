#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>

#include "root.h"

const char* ssid = "........";
const char* password = "........";
WebServer server(80);
int temp = 22;
int pressure = 1013;

void sendPage()
{
  String value(temp);
  String message;
  message += webRoot;
  message.replace("%TEMP%", value);
  value = pressure;
  message.replace("%PRESSURE%", value);
  server.send(200, "text/html", message);
}

void handleRoot(void)
{
  sendPage();
}

void handleTup(void)
{
  temp++;
  sendPage();
}

void handleTdown(void)
{
  temp--;
  sendPage();
}

void handlePup(void)
{
  pressure++;
  sendPage();
}

void handlePdown(void)
{
  pressure--;
  sendPage();
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void)
{
  char hostName[12];
  uint8_t mac[6];

  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
  WiFi.macAddress(mac);
  sprintf(hostName, "Esp32%x%x%x", mac[3], mac[4], mac[5]);
  Serial.printf("setting hostname %s: %d\n", hostName, WiFi.setHostname(hostName));
  Serial.print("Connecting to "); Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println(""); Serial.printf("%s (%s): connected to %s", WiFi.getHostname(), WiFi.macAddress().c_str(), ssid);
  Serial.print("IP address: "); Serial.println(WiFi.localIP());
  if (MDNS.begin(hostName)) {
    Serial.println("MDNS responder started");
  }
  server.on("/", handleRoot);
  server.on("/tup", handleTup);
  server.on("/tdown", handleTdown);
  server.on("/pup", handlePup);
  server.on("/pdown", handlePdown);
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("HTTP server started");
}

void loop()
{
  server.handleClient();
}
