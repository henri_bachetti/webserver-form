#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>

const char* ssid = "........";
const char* password = "........";

#define FORM \
  "<!DOCTYPE html>\n" \
  "<html>\n" \
  "<head>\n" \
  "<meta charset=\"UTF-8\">\n" \
  "<title>A simple form</title>\n" \
  "</head>\n" \
  "<body>\n" \
  "<form action=\"/form-handle\" method=\"post\">\n" \
  "<br>" \
  "<label for=\"name\">Name : </label>\n" \
  "<br>" \
  "<input type=\"text\" id=\"name\" name=\"user_name\">\n" \
  "<br>" \
  "<label for=\"mail\">email : </label>\n" \
  "<br>" \
  "<input type=\"email\" id=\"mail\" name=\"user_mail\">\n" \
  "<br>" \
  "<label for=\"msg\">Message : </label>\n" \
  "<br>" \
  "<textarea id=\"msg\" name=\"user_message\"></textarea>\n" \
  "<br>" \
  "<br>" \
  "<input type=\"submit\" value=\"Send\">\n" \
  "</form>\n" \
  "</body>\n" \
  "</html>\n"

#define SENSOR_FORM \
  "<!DOCTYPE html>\n" \
  "<html>\n" \
  "<head>\n" \
  "<meta charset=\"UTF-8\">\n" \
  "<title>A sensor form</title>\n" \
  "</head>\n" \
  "<body>\n" \
  "<form action=\"/sensor-form-handle\" method=\"post\">\n" \
  "<br>" \
  "Sensor :<br>" \
  "<table>" \
  "<tr>" \
  "<td>\n" \
  "<input type=\"checkbox\" name=\"temperature\" value=\"temperature\" checked>Temperature\n" \
  "</td>" \
  "<td>\n" \
  "<input type=\"radio\" name=\"temp_sensor\" value=\"temp_ds18b20\" checked>DS18B20\n" \
  "</td>" \
  "<td>\n" \
  "<input type=\"radio\" name=\"temp_sensor\" value=\"temp_mcp9808\">MCP9808\n" \
  "</td>" \
  "<td>\n" \
  "<input type=\"radio\" name=\"temp_sensor\" value=\"temp_dht22\">DHT22\n" \
  "</td>" \
  "<td>\n" \
  "<input type=\"radio\" name=\"temp_sensor\" value=\"temp_sht31\">SHT31<br>\n" \
  "</td>" \
  "</tr>" \
  "<tr>" \
  "<td>\n" \
  "<input type=\"checkbox\" name=\"humidity\" value=\"humidity\" checked>Humidity\n" \
  "</td>" \
  "<td>\n" \
  "<input type=\"radio\" name=\"hum_sensor\" value=\"hum_dht22\" checked>DHT22\n" \
  "</td>" \
  "<td>\n" \
  "<input type=\"radio\" name=\"hum_sensor\" value=\"hum_sht31\">SHT31<br>\n" \
  "</td>" \
  "<td>" \
  "</td>" \
  "<td>" \
  "</tr>" \
  "<tr>" \
  "<td>\n" \
  "<input type=\"checkbox\" name=\"pressure\" value=\"pressure\" checked>Pressure\n" \
  "</td>" \
  "<td>\n" \
  "<input type=\"radio\" name=\"press_sensor\" value=\"press_bmp180\" checked>BMP180\n" \
  "</td>" \
  "<td>\n" \
  "<input type=\"radio\" name=\"press_sensor\" value=\"press_bme280\">BME280\n" \
  "</td>" \
  "<td>" \
  "</td>" \
  "<td>\n" \
  "</table>" \
  "<br>\n" \
  "<label for=\"msg\">Minimal temperature : </label>\n" \
  "<br>\n" \
  "<input type=\"number\" id=\"temp_min\" name=\"temp_min\"  value=\"%d\" min=\"15\" max=\"25\">\n" \
  "<br>\n" \
  "<label for=\"msg\">Maximal temperature : </label>" \
  "<br>\n" \
  "<input type=\"number\" id=\"temp_max\" name=\"temp_max\"  value=\"%d\" min=\"15\" max=\"25\">\n" \
  "<br>\n" \
  "<label for=\"msg\">Minimal humidity : </label>" \
  "<br>\n" \
  "<input type=\"number\" id=\"hum_min\" name=\"hum_min\"  value=\"%d\" min=\"30\" max=\"80\">\n" \
  "<br>\n" \
  "<label for=\"msg\">Maximal humidity : </label>" \
  "<br>\n" \
  "<input type=\"number\" id=\"hum_max\" name=\"hum_max\"  value=\"%d\" min=\"30\" max=\"80\">\n" \
  "<br>\n" \
  "<label for=\"msg\">Minimal pressure : </label>" \
  "<br>\n" \
  "<input type=\"number\" id=\"press_min\" name=\"press_min\"  value=\"%d\" min=\"500\" max=\"2000\">\n" \
  "<br>\n" \
  "<label for=\"msg\">Maximal pressure : </label>" \
  "<br>\n" \
  "<input type=\"number\" id=\"press_max\" name=\"press_max\"  value=\"%d\" min=\"500\" max=\"2000\">\n" \
  "<br>\n" \
  "<br>\n" \
  "<input type=\"submit\" value=\"Send\">\n" \
  "</form>\n" \
  "</body>\n" \
  "</html>\n"

WebServer server(80);

const int led = 13;

void handleParser() {
  String message;
  message += "Method: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nURI: ";
  message += server.uri();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(200, "text/plain", message);
}

void handleForm() {
  String message;
  message += FORM;
  server.send(200, "text/html", message);
}

void handleFormData() {
  String message;
  message += "user_name=" + server.arg("user_name") + "<br>";
  message += "user_mail=" + server.arg("user_mail") + "<br>";
  message += "user_message=" + server.arg("user_message") + "<br>";
  server.send(200, "text/html", message);
}

char buf[4000];

void handleSensorForm() {
  sprintf(buf, SENSOR_FORM, 19, 21, 39, 43, 1000, 1100);
  server.send(200, "text/html", buf);
}

void handleSensorFormData() {
  String message;
  message += "temperature : ";
  message += server.hasArg("temperature") ? "YES" : "NO";
  message += "<br>";
  message += "humidity : ";
  message += server.hasArg("humidity") ? "YES" : "NO";
  message += "<br>";
  message += "pressure : ";
  message += server.hasArg("pressure") ? "YES" : "NO";
  message += "<br>";
  message += "<br>";
  message += "Temperature sensor: " + server.arg("temp_sensor") + "<br>";
  message += "Humidity sensor: " + server.arg("hum_sensor") + "<br>";
  message += "Pressure sensor: " + server.arg("press_sensor") + "<br>";
  message += "<br>";
  message += "Minimal temperature : " + server.arg("temp_min") + "<br>";
  message += "Maximal temperature : " + server.arg("temp_max") + "<br>";
  message += "Minimal humidity : " + server.arg("hum_min") + "<br>";
  message += "Maximal humidity : " + server.arg("hum_max") + "<br>";
  message += "Minimal pressure : " + server.arg("press_min") + "<br>";
  message += "Maximal pressure : " + server.arg("press_max") + "<br>";
  server.send(200, "text/html", message);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void)
{
  char hostName[12];
  uint8_t mac[6];

  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE);
  WiFi.macAddress(mac);
  sprintf(hostName, "Esp32%x%x%x", mac[3], mac[4], mac[5]);
  Serial.printf("setting hostname %s: %d\n", hostName, WiFi.setHostname(hostName));
  Serial.print("Connecting to "); Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.printf("%s (%s): connected to %s", WiFi.getHostname(), WiFi.macAddress().c_str(), ssid);
  Serial.print("IP address: "); Serial.println(WiFi.localIP());
  if (MDNS.begin(hostName)) {
    Serial.println("MDNS responder started");
  }

  server.on("/parser", handleParser);

  server.on("/form", handleForm);

  server.on("/form-handle", handleFormData);

  server.on("/sensor-form", handleSensorForm);

  server.on("/sensor-form-handle", handleSensorFormData);

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
}
