
#include <SPI.h>
#include <Ethernet.h>

#define GET                   1
#define POST                  2
#define FORM                  POST

#define REQUEST_MAX           150
#define URL_MAX               100
#define ARGSTR_MAX            80
#define ARG_MAX               5

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xAA, 0xBB, 0xCC
};

EthernetServer server(80);

#if FORM == GET
#define theForm "<form action=\"/form-handle\" method=\"get\">" \
  "<label for=\"name\">Name : </label>\n" \
  "<br>" \
  "<input type=\"text\" id=\"name\" name=\"user_name\">\n" \
  "<br>" \
  "<label for=\"mail\">email : </label>" \
  "<br>" \
  "<input type=\"email\" id=\"mail\" name=\"user_mail\">\n" \
  "<br>" \
  "<label for=\"msg\">Message : </label>" \
  "<br>" \
  "<textarea id=\"msg\" name=\"user_message\"></textarea>\n" \
  "<br>" \
  "<br>" \
  "<input type=\"submit\" value=\"Send\">\n" \
  "</form>"
#else
#define theForm "<form action=\"/form-handle\" method=\"post\">\n" \
  "<br>" \
  "<label for=\"name\">Name : </label>\n" \
  "<br>" \
  "<input type=\"text\" id=\"name\" name=\"user_name\">\n" \
  "<br>" \
  "<label for=\"mail\">email : </label>" \
  "<br>" \
  "<input type=\"email\" id=\"mail\" name=\"user_mail\">\n" \
  "<br>" \
  "<label for=\"msg\">Message : </label>" \
  "<br>" \
  "<textarea id=\"msg\" name=\"user_message\"></textarea>\n" \
  "<br>" \
  "<br>" \
  "<input type=\"submit\" value=\"Send\">\n" \
  "</form>"
#endif

struct cgi_arg
{
  char *name;
  char *value;
};

void setup()
{
  Serial.begin(115200);
  Serial.println(F("CGI args Example"));
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    } else if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // no point in carrying on, so do nothing forevermore:
    while (true) {
      delay(1);
    }
  }
  server.begin();
  Serial.print(F("server is at "));
  Serial.println(Ethernet.localIP());
}

char *_type;
char _fullUrl[URL_MAX];
char *_url;
char *_proto;
char *_ip;
char *_agent;
struct cgi_arg _args[ARG_MAX];

unsigned char h2int(char c)
{
  if (c >= '0' && c <= '9') return ((unsigned char)c - '0');
  if (c >= 'a' && c <= 'f') return ((unsigned char)c - 'a' + 10);
  if (c >= 'A' && c <= 'F') return ((unsigned char)c - 'A' + 10);
  return (0);
}

/*
   decode escaped chars like %20, etc
*/
const char *decode(const char *src, char *dest, size_t size)
{
  size_t i = 0;
  char code0;
  char code1;

  while (*src++) {
    if (*src == '%' && *(src + 1) != 0 && *(src + 2) != 0) {
      src++;
      code0 = *src;
      src++;
      code1 = *src;
      if (i < size) {
        dest[i++] = (h2int(code0) << 4) | h2int(code1);
      }
    }
    else {
      if (i < size) {
        dest[i++] = *src;
      }
    }
  }
  dest[i] = '\0';
  return dest;
}

/*
   return argument value for a given name
*/
const char *getArg(const char *name)
{
  for (int i = 0 ; i < ARG_MAX ; i++) {
    if (_args[i].name && !strcmp(_args[i].name, name)) {
      return _args[i].value;
    }
  }
  return NULL;
}

/*
   parse HTTP request
*/
void parseRequest(char *request, int mode, char *argsStr)
{
  char *urlStart;

  char *token = strtok(request, " ");
  if (token != NULL) {
    _type = token;
  }
  token = strtok(NULL, " ");
  if (token != NULL) {
    urlStart = token;
    strcpy(_fullUrl, token);
    Serial.print(F("URL : ")); Serial.println(_fullUrl);
  }
  token = strtok(NULL, "/");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _proto = token;
  }
  token = strtok(NULL, " ");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _ip = token;
  }
  token = strtok(NULL, " ");
  token = strtok(NULL, "\n");
  if (token != NULL) {
    _agent = token;
  }
  token = strtok(urlStart, "?");
  if (token != NULL) {
    _url = token;
  }
  memset(_args, 0, sizeof(_args));
  if (mode == POST) {
    token = strtok(argsStr, "=");
  }
  else {
    token = strtok(NULL, "=");
  }
  int i = 0;
  while (i < ARG_MAX && token != NULL) {
    _args[i].name = token;
    token = strtok(NULL, "&");
    if (token != NULL) {
      _args[i++].value = token;
    }
    token = strtok(NULL, "=");
  }
  for (int i = 0 ; i < ARG_MAX ; i++) {
    if (_args[i].name) {
      Serial.print(_args[i].name); Serial.print(F("=")); Serial.println(_args[i].value);
    }
  }
}

void handleNotFound(EthernetClient &client)
{
  IPAddress addr = Ethernet.localIP();
  char ip[16];

  Serial.println(F("http_request::handleNotFound"));
  for (int i = 0; i < 4 ; i++) {
    sprintf(ip, "%d.%d.%d.%d", addr[0], addr[1], addr[2], addr[3]);
  }
  const char *htmlHeader = "<!DOCTYPE HTML>\n<html><head>\n<title>404 Not Found</title>\n</head><body>\n";
  client.println(htmlHeader);
  client.println("<h1>Not Found</h1>\n<p>The requested URL ");
  client.print(_url);
  client.println(" was not found on this server.</p>\n<hr>\n<address>Arduino Server at ");
  client.print(ip); client.print(" Port "); client.print(client.localPort());
  client.print("</address>\n</body></html>");
}

void handleGetRequest(EthernetClient &client, char *request)
{
  parseRequest(request, GET, 0);
  if (strcmp(_url, "/parser") == 0) {
    client.println(F("HTTP/1.1 200 OK"));
    client.println(F("Content-Type: text/html"));
    client.println(F("Connection: close"));  // the connection will be closed after completion of the response
    client.println();
    client.println(F("<!DOCTYPE HTML>"));
    client.println(F("<html>"));
    client.print(F("Type: ")); client.print(_type); client.println(F("<br/>"));
    client.print(F("Full URL: ")); client.print(_fullUrl); client.println(F("<br/>"));
    client.print(F("URL: ")); client.print(_url); client.println(F("<br/>"));
    client.print(F("PROTO: ")); client.print(_proto); client.println(F("<br/>"));
    client.print(F("IP: ")); client.print(_ip); client.println(F("<br/>"));
    client.print(F("PORT: ")); client.print(client.localPort()); client.println(F("<br/>"));
    client.print(F("REMOTE IP: ")); client.print(client.remoteIP()); client.println(F("<br/>"));
    client.print(F("REMOTE PORT: ")); client.print(client.remotePort()); client.println(F("<br/>"));
    client.print(F("AGENT: ")); client.print(_agent); client.println(F("<br/>"));
    client.println(F("<br/>"));
    for (int i = 0 ; i < ARG_MAX ; i++) {
      if (_args[i].name) {
        client.print(_args[i].name); client.print(F(" = ")); client.print(_args[i].value);
        client.println(F("<br/>"));
      }
    }
    client.println(F("</html>"));
  }
  else if (strcmp(_url, "/form") == 0) {
    client.println(F("HTTP/1.1 200 OK"));
    client.println(F("Content-Type: text/html"));
    client.println(F("Connection: close"));
    client.println();
    client.println(F("<!DOCTYPE HTML>"));
    client.println(F("<html>"));
    client.print(F(theForm));
    client.println(F("</html>"));
  }
  else if (strcmp(_url, "/form-handle") == 0) {
    char buf[50];
    client.println(F("HTTP/1.1 200 OK"));
    client.println(F("Content-Type: text/html"));
    client.println(F("Connection: close"));
    client.println();
    client.println(F("<!DOCTYPE HTML>"));
    client.println(F("<html>"));
    client.print("user_name"); client.print(F(" = ")); client.print(getArg("user_name"));
    client.println(F("<br/>"));
    // decode the name%40server to name@server
    client.print("user_mail"); client.print(F(" = ")); client.print(decode(getArg("user_mail"), buf, 50));
    client.println(F("<br/>"));
    client.print("user_message"); client.print(F(" = ")); client.print(getArg("user_message"));
    client.println(F("<br/>"));
    client.println(F("</html> "));
  }
  else {
    handleNotFound(client);
  }
}

void handlePostRequest(EthernetClient &client, char *request, char *argStr)
{
  parseRequest(request, POST, argStr);
  if (strcmp(_url, "/form-handle") == 0) {
    char buf[50];
    client.println(F("HTTP/1.1 200 OK"));
    client.println(F("Content-Type: text/html"));
    client.println(F("Connection: close"));
    client.println();
    client.println(F("<!DOCTYPE HTML > "));
    client.println(F("<html>"));
    client.print("user_name"); client.print(F(" = ")); client.print(getArg("user_name"));
    client.println(F("<br/>"));
    // decode the name%40server to name@server
    client.print("user_mail"); client.print(F(" = ")); client.print(decode(getArg("user_mail"), buf, 50));
    client.println(F("<br/>"));
    client.print("user_message"); client.print(F(" = ")); client.print(getArg("user_message"));
    client.println(F("<br/>"));
    client.println(F("</html> "));
  }
  else {
    handleNotFound(client);
  }
}

void loop()
{
  static char request[REQUEST_MAX + 1];
  int reqIndex = 0;
  char c;
  EthernetClient client = server.available();

  if (client) {
    Serial.println(F("new client"));
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        c = client.read();
        if (reqIndex < REQUEST_MAX) {
          request[reqIndex++] = c;
          request[reqIndex] = 0;
        }
        if (c == '\n' && currentLineIsBlank && !strncmp(request, "GET", 3)) {
          Serial.print(F("Total - Length = ")); Serial.println(reqIndex);
          Serial.println(request);
          handleGetRequest(client, request);
          break;
        }
        if (c == '\n' && currentLineIsBlank && !strncmp(request, "POST", 4)) {
          char argStr[ARGSTR_MAX + 1];
          int i = 0;
          while (c > 0) {
            c = client.read();
            if (c > 0 && i < ARGSTR_MAX) {
              argStr[i++] = c;
              argStr[i] = 0;
            }
            else {
              Serial.println(F("buffer is OVER"));
            }
          }
          Serial.println(request);
          Serial.print(F("Total - Length = ")); Serial.println(reqIndex);
          Serial.print("ARGS = "); Serial.println(argStr);
          handlePostRequest(client, request, argStr);
          break;
        }
      }
      if (c == '\n') {
        currentLineIsBlank = true;
      }
      else if (c != '\r') {
        currentLineIsBlank = false;
      }
    }
    delay(1);
    client.stop();
    Serial.println(F("client disconnected"));
  }
}
