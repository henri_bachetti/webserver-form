# ARDUINO WEB SERVER WITH FORMS

The purpose of this page is to explain the implementation of a WEB server able to display forms.

The project uses the following components :

 * an ARDUINO UNO or MEGA with a W5100 Ethernet board
 * or
 * an ESP32 or ESP8266

There is 3 Projects here :

 * Ethernet project with forms
 * ESP32 or ESP8266 project with forms
 * an ESP32 or ESP8266 subscribers server with forms, RC522 RFID reader, HTML files on SPIFFS, and user data in FAT

The subscriber server can be remote tested using PYTHON3 and unitttest (see test directory)
 
### ARDUINO

The code is build using ARDUINO IDE 1.8.11.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/03/arduino-ethernet-esp32-comparons.html

The subscribers server : https://riton-duino.blogspot.com/2020/03/serveur-esp32-implementation-et-test.html

