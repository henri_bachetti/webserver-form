
#include <WiFi.h>
#include <WebServer.h>

// SSID & Password
const char* ssid     = "ESP32-Access-Point";
const char* password = "123456789";

WebServer server(80);  // Object of WebServer(HTTP port, 80 is defult)

void setup() {
  Serial.begin(115200);
  Serial.print("Setting AP on ");
  Serial.println(ssid);

  WiFi.softAP(ssid, password);
  
  Serial.println("WiFi started successfully");
  Serial.print("Got IP: ");
  Serial.println(WiFi.softAPIP());

  server.on("/", handle_root);

  server.begin();
  Serial.println("HTTP server started");
  delay(100); 
}

void loop() {
  server.handleClient();
}

// HTML & CSS contents which display on web server
String HTML = "<!DOCTYPE html>\
<html>\
<body>\
<h1>My First Web Server with ESP32 - Station Mode &#128522;</h1>\
</body>\
</html>";

// Handle root url (/)
void handle_root() {
  server.send(200, "text/html", HTML);
}
